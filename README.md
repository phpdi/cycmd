# cycmd

#### 介绍
存放工具集

### word
该子命令支持各种单词格式转换  
Usage:  
word [flags]  

Flags:  
-h, --help         help for word  
-m, --mode int8    请输入单词转换的模式  
-s, --str string   请输入单词内容  


-m  
默认:输出所有  
1：全部转大写  
2：全部转小写  
3：下划线转大写驼峰  
4：下划线转小写驼峰  
5：驼峰转下划线  

例如: cycmd word -m 1 -s aaa      
2021/09/11 10:21:34 输出结果: AAA  

### time
时间格式处理

Usage:  
time [flags]  
time [command]  

Available Commands:  
calc        计算所需时间  
now         获取当前时间  

Flags:  
-h, --help   help for time  

例如:  cycmd time  now  
2021/09/11 10:21:02 输出结果: 2021-09-11 10:21:02, 1631326862  


### t2go
数据表转换为 结构体、proto、消息体、字段定义、dto转换函数  
表结构转换为go结构体：  
-d:root:123456@(192.168.49.2:32316)/pricing?charset=utf8  
-t:table_name  

Usage:  
t2go [flags]  

Flags:  
-d, --dns string         数据库连接信息 例如: root:123456@(192.168.49.2:32316)/pricing?charset=utf8  
-h, --help               help for t2go  
-t, --tableName string   表名  

例如:   
cycmd t2go -d root:123456@(192.168.49.2:32316)/pricing?charset=utf8 -t tablename  


### t2gotpl
将表结合模板转换为go代码：   
-d:root:123456@(192.168.49.2:32316)/pricing?charset=utf8  
-t:table_name  
-w:work_dir  

Usage:  
t2gotpl [flags]  

Flags:  
-d, --dns string         数据库连接信息 例如: root:123456@(192.168.49.2:32316)/pricing?charset=utf8  
-h, --help               help for t2gotpl  
-t, --tableName string   表名  
-w, --workDir string     工作路径 (default "./")  

例如:   
cycmd t2gotpl -d root:123456@(192.168.49.2:32316)/pricing?charset=utf8 -t tablename  -w ./template
