package cmd

import (
	"fmt"
	"gitee.com/phpdi/cycmd/internal/t2go"
	"github.com/spf13/cobra"
	"log"
	"strings"
)

var (
	tableName  string
	dns        string
	workDir    string
	suffix     string
	delimsType int
	t2gotplCmd = &cobra.Command{
		Use:   "t2go",
		Short: "将表结合模板转换为go代码",
		Long: strings.Join([]string{
			"将表结合模板转换为go代码：",
			"-d:root:123456@(192.168.49.2:32316)/pricing?charset=utf8",
			"-t:table_name",
			"-w:work_dir",
		}, "\n"),
		Run: func(cmd *cobra.Command, args []string) {
			ti := t2go.NewMysqlTableInfo(dns)
			tgt := t2go.NewT2GoTpl(ti, &t2go.Config{
				TableName:  tableName,
				WorkDir:    workDir,
				Suffix:     suffix,
				DelimsType: delimsType,
			})
			if err := tgt.Run(); err != nil {
				log.Fatalf("错误: %s", err)
			}

			fmt.Println("go code generate success")
		},
	}
)

func init() {
	t2gotplCmd.Flags().StringVarP(&dns, "dns", "d", "", "数据库连接信息 例如: root:123456@(192.168.49.2:32316)/pricing?charset=utf8")
	t2gotplCmd.Flags().StringVarP(&tableName, "tableName", "t", "", "表名")
	t2gotplCmd.Flags().StringVarP(&workDir, "workDir", "w", "./", "工作路径")
	t2gotplCmd.Flags().StringVarP(&suffix, "suffix", "s", ".go", "文件后缀")
	t2gotplCmd.Flags().IntVarP(&delimsType, "delimsType", "", 0, "模板标签界定符号")
}
