package router

import (
	"gitee.com/zhangfei_/city-express-api/app/admin/apis"
	"github.com/gin-gonic/gin"
	jwt "github.com/go-admin-team/go-admin-core/sdk/pkg/jwtauth"
)

func init() {
	routerCheckRole = append(routerCheckRole, register{{.EntityName}})
}

func register{{.EntityName}}(v1 *gin.RouterGroup, authMiddleware *jwt.GinJWTMiddleware) {
	api := apis.{{.EntityName}}{}

	v1.Group("/exp-distribution-level").
		Use(authMiddleware.MiddlewareFunc()).
		GET("/:id", api.Get{{.EntityName}}).
		GET("", api.Page{{.EntityName}}).
		POST("", api.Create{{.EntityName}}).
		PATCH("", api.Update{{.EntityName}}).
		DELETE("", api.Delete{{.EntityName}})
}
