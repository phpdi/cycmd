package tables

import (
	"github.com/GoAdminGroup/go-admin/context"
	"github.com/GoAdminGroup/go-admin/modules/db"
	"github.com/GoAdminGroup/go-admin/plugins/admin/modules/table"
	"github.com/GoAdminGroup/go-admin/template/types/form"
)

var (
     {{range .Fields}}{{if .Comment.Option}}{{.Comment.OptionContent}} //{{.Comment.Name}}
     {{end}}{{end}}
)
// Get{{.EntityName}}Table {{.EntityTitle}}
func Get{{.EntityName}}Table(ctx *context.Context) table.Table {

	{{.EntityName|LowerFirstLetter}} := table.NewDefaultTable(table.DefaultConfigWithDriver("mysql"))

	info := {{.EntityName|LowerFirstLetter}}.GetInfo().HideFilterArea().HideCheckBoxColumn().HideDeleteButton().HideNewButton().HideExportButton().HideEditButton()
	commonFilter(info)

	info.AddField("{{.PkField.Comment.Name}}", "{{.PkField.DBKey}}", db.{{.PkField.SourceType|UpperFirstLetter}}).FieldFilterable()
	{{range .Fields}}{{if .Comment.Option}}info.AddField("{{.Comment.Name}}", "{{.DBKey}}", db.{{.SourceType|UpperFirstLetter}}).FieldDisplay(option({{.Comment.OptionVar}})){{else}}info.AddField("{{.Comment.Name}}", "{{.DBKey}}", db.{{.SourceType|UpperFirstLetter}}){{end}}
	{{end}}
	info.SetTable("{{.TableName}}").SetTitle("{{.EntityTitle}}").SetDescription("{{.EntityTitle}}")

	formList := {{.EntityName|LowerFirstLetter}}.GetForm()
	formList.AddField("{{.PkField.Comment.Name}}", "{{.PkField.DBKey}}", db.{{.PkField.SourceType|UpperFirstLetter}}, form.Default)
	{{range .Fields}}{{if .Comment.Option}}formList.AddField("{{.Comment.Name}}", "{{.DBKey}}", db.{{.SourceType|UpperFirstLetter}},form.SelectSingle).FieldOptions(optionForm({{.Comment.OptionVar}})){{else}}formList.AddField("{{.Comment.Name}}", "{{.DBKey}}", db.{{.SourceType|UpperFirstLetter}},{{.FromType}}){{end}}
	{{end}}
	formList.SetTable("{{.TableName}}").SetTitle("{{.EntityTitle}}").SetDescription("{{.EntityTitle}}")

	return {{.EntityName|LowerFirstLetter}}


}
