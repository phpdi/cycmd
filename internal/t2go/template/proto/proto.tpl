syntax = "proto3";

package production;

option go_package = ".;pb";

import "google/protobuf/wrappers.proto";
import "google/protobuf/timestamp.proto";
import "google/api/annotations.proto";
import "protoc-gen-openapiv2/options/annotations.proto";
import "common.proto";

//公共服务
service CooperateManager {

  rpc Describe{{.EntityName}} (Describe{{.EntityName}}Request) returns (Describe{{.EntityName}}Response) {
    option (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_operation) = {
      summary: "{{.EntityTitle}}-查询"
    };
    option (google.api.http) = {
      get: "/v1/{{.UrlName}}",
    };
  }

  rpc Create{{.EntityName}} (Create{{.EntityName}}Request) returns (Create{{.EntityName}}Response) {
    option (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_operation) = {
      summary: "{{.EntityTitle}}-新增"
    };
    option (google.api.http) = {
      post: "/v1/{{.UrlName}}",
      body:"*"
    };
  }

  rpc Update{{.EntityName}} (Update{{.EntityName}}Request) returns (Update{{.EntityName}}Response) {
    option (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_operation) = {
      summary: "{{.EntityTitle}}-修改"
    };
    option (google.api.http) = {
      patch: "/v1/{{.UrlName}}",
      body:"*"
    };
  }

  rpc Delete{{.EntityName}} (Del{{.EntityName}}Request) returns (Del{{.EntityName}}Response) {
    option (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_operation) = {
      summary: "{{.EntityTitle}}-删除"
    };
    option (google.api.http) = {
      delete: "/v1/{{.UrlName}}",
      body:"*"
    };
  }

}



//--------------------------------实体映射开始-------------------------------------



// {{.EntityName}}
message {{.EntityName}} {
  {{range $k,$v:=.FullFields}}//{{$v.Comment.Comment}}{{if $v.Comment.Option}} @gotags:valid:"OneOf({{$v.Comment.OptionKey|Join}})"{{end}}
  {{$v.ProtoType}} {{$v.DBKey}}={{AddInt $k 1}};
  {{end}}
}

//--------------------------------实体映射结束-------------------------------------



//查询{{.EntityTitle}}
message Describe{{.EntityName}}Request{
  //{{.PkField.Comment.Comment}}
  repeated {{.PkField.ProtoType}} {{.PkField.DBKey}}=1;
}
message Describe{{.EntityName}}Response{
  repeated {{.EntityName}} data=1;
}

//新增{{.EntityTitle}}
message Create{{.EntityName}}Request{
  {{range $k,$v:=.Fields}}//{{$v.Comment.Comment}}{{if $v.Comment.Option}}@gotags:valid:"OneOf({{$v.Comment.OptionKey|Join}})"{{end}}
  {{$v.ProtoType}} {{$v.DBKey}}={{AddInt $k 1}};
  {{end}}
}
message Create{{.EntityName}}Response{
}


//修改{{.EntityTitle}}
message Update{{.EntityName}}Request{
  //{{.PkField.Comment.Comment}}
  {{.PkField.ProtoType}} {{.PkField.DBKey}}=1;{{range $k,$v:=.Fields}}
  //{{$v.Comment.Comment}}{{if $v.Comment.Option}}@gotags:valid:"OneOf({{$v.Comment.OptionKey|Join}})"{{end}}
  {{$v.ProtoType}} {{$v.DBKey}}={{AddInt $k 2}};{{end}}
}
message Update{{.EntityName}}Response{
}

//删除{{.EntityTitle}}
message Del{{.EntityName}}Request{
  //{{.PkField.Comment.Comment}} @gotags:valid:"Required"
  repeated {{.PkField.ProtoType}} {{.PkField.DBKey}}=1;
}
message Del{{.EntityName}}Response{
}

