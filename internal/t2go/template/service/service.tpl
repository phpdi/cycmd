package service

import (
	"context"
	"gitee.com/zhangfei_/city-express-api/app/admin/models"
	"gitee.com/zhangfei_/city-express-api/app/admin/service/dto"
	"gitee.com/zhangfei_/city-express-api/pkg/util/jsonutil"
	"github.com/go-admin-team/go-admin-core/sdk/service"
)

// {{.EntityName}} {{.EntityTitle}}服务
type {{.EntityName}} struct {
	service.Service
}

// Create{{.EntityName}} {{.EntityTitle}}创建
func (d {{.EntityName}}) Create{{.EntityName}}(ctx context.Context, req dto.Create{{.EntityName}}Req) (ack dto.Create{{.EntityName}}Ack, err error) {
	var (
		e models.{{.EntityName}}
	)
	if err = jsonutil.StructCopy(req, &e); err != nil {
		return
	}

	if err = d.Orm.Create(&e).Error; err != nil {
		return
	}

	return
}

// Update{{.EntityName}} {{.EntityTitle}}创建
func (d {{.EntityName}}) Update{{.EntityName}}(ctx context.Context, req dto.Update{{.EntityName}}Req) (ack dto.Update{{.EntityName}}Ack, err error) {
	var (
		e models.{{.EntityName}}
	)
	if err = jsonutil.StructCopy(req, &e); err != nil {
		return
	}

	if err = d.Orm.Updates(&e).Error; err != nil {
		return
	}

	return
}

// Delete{{.EntityName}} {{.EntityTitle}}删除
func (d {{.EntityName}}) Delete{{.EntityName}}(ctx context.Context, req dto.Delete{{.EntityName}}Req) (ack dto.Delete{{.EntityName}}Ack, err error) {
	err = d.Orm.Where("id=? and sys_user_id=?", req.Id, req.SysUserId).Delete(&models.{{.EntityName}}{}).Error
	return
}

// Get{{.EntityName}} {{.EntityTitle}}
func (d {{.EntityName}}) Get{{.EntityName}}(ctx context.Context, req dto.Get{{.EntityName}}Req) (ack models.{{.EntityName}}, err error) {
	err = d.Orm.Where("id=? and sys_user_id=?", req.Id, req.SysUserId).First(&ack).Error
	return
}

// Page{{.EntityName}} {{.EntityTitle}}分页
func (d {{.EntityName}}) Page{{.EntityName}}(ctx context.Context, req dto.Page{{.EntityName}}Req) (ack dto.Page{{.EntityName}}Ack, err error) {
	err = d.Orm.Where("sys_user_id=?", req.SysUserId).First(&ack.List).Error
	return
}
