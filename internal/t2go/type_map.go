package t2go

var (

	// goMapProto go字段类型映射成proto类型
	goMapProto = map[string]string{
		"int":       "int32",
		"int64":     "int64",
		"bool":      "bool",
		"time.Time": "google.protobuf.Timestamp",
		"float64":   "double",
		"float32":   "float",
		"string":    "string",
	}

	// goMapForm go结构体映射成表单类型
	goMapForm = map[string]string{
		"int":       "form.Number",
		"int64":     "form.Number",
		"bool":      "form.Bool",
		"time.Time": "form.Datetime",
		"float64":   "form.Number",
		"float32":   "form.Number",
		"string":    "form.Text",
	}
)
