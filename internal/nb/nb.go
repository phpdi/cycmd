package nb

import (
	"encoding/json"
	"fmt"
	"log"
	"time"

	"github.com/golang/protobuf/ptypes/wrappers"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

type AttrIdValue struct {
	//产品属性ID,兼容code.
	AttrId *wrappers.StringValue `protobuf:"bytes,1,opt,name=attr_id,json=attrId,proto3" json:"attr_id,omitempty"`
	//产品属性值
	AttrValue            *wrappers.StringValue `protobuf:"bytes,2,opt,name=attr_value,json=attrValue,proto3" json:"attr_value,omitempty"`
	XXX_NoUnkeyedLiteral struct{}              `json:"-"`
	XXX_unrecognized     []byte                `json:"-"`
	XXX_sizecache        int32                 `json:"-"`
}

type ComponentInfo struct {
	// 计费项ID
	CompId *wrappers.StringValue `protobuf:"bytes,1,opt,name=comp_id,json=compId,proto3" json:"comp_id,omitempty"`
	//计费项的属性键值对 - 计费项由一组产品属性和属性值组成。
	Attrs                []*AttrIdValue `protobuf:"bytes,2,rep,name=attrs,proto3" json:"attrs,omitempty"`
	XXX_NoUnkeyedLiteral struct{}       `json:"-"`
	XXX_unrecognized     []byte         `json:"-"`
	XXX_sizecache        int32          `json:"-"`
}

type PlanWithAttrsV2 struct {
	// 计费方案的ID
	PlanId *wrappers.StringValue `protobuf:"bytes,1,opt,name=plan_id,json=planId,proto3" json:"plan_id,omitempty"`
	//计费项 - 计费方案由不同的计费项组成，一个计费方案可以有多个计费项。
	Components           []*ComponentInfo `protobuf:"bytes,2,rep,name=components,proto3" json:"components,omitempty"`
	XXX_NoUnkeyedLiteral struct{}         `json:"-"`
	XXX_unrecognized     []byte           `json:"-"`
	XXX_sizecache        int32            `json:"-"`
}

type ProdInstanceExt struct {
	//产品实例ID
	Id string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	//是否自动续订- 0：否，1：是
	IsAutoRenew          int32    `protobuf:"varint,2,opt,name=is_auto_renew,json=isAutoRenew,proto3" json:"is_auto_renew,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

type OrderProductV2 struct {
	//产品ID
	ProdId *wrappers.StringValue `protobuf:"bytes,1,opt,name=prod_id,json=prodId,proto3" json:"prod_id,omitempty"`
	//产品名称
	ProdName *wrappers.StringValue `protobuf:"bytes,2,opt,name=prod_name,json=prodName,proto3" json:"prod_name,omitempty"`
	//外部产品实例ID，多个代表多个实例
	ProdInstanceExtArray []*ProdInstanceExt `protobuf:"bytes,3,rep,name=prod_instance_ext_array,json=prodInstanceExtArray,proto3" json:"prod_instance_ext_array,omitempty"`
	//计费方案
	Plan                 *PlanWithAttrsV2 `protobuf:"bytes,4,opt,name=plan,proto3" json:"plan,omitempty"`
	XXX_NoUnkeyedLiteral struct{}         `json:"-"`
	XXX_unrecognized     []byte           `json:"-"`
	XXX_sizecache        int32            `json:"-"`
}

//go run main.go nb -d="root:p@ss52Dnb@tcp(192.168.49.2:32316)/subscription?charset=utf8mb4&parseTime=true"
type ProdInstance struct {
	ProdInstId     string    `json:"prod_inst_id" gorm:"primaryKey"` // 产品实例ID
	AccessSysId    string    `json:"access_sys_id"`                  // 接入系统ID
	OrderId        string    `json:"order_id"`                       // 订单ID
	ProdId         string    `json:"prod_id"`                        // 产品ID
	ProdCode       string    `json:"prod_code"`                      // 产品编码
	ProdName       string    `json:"prod_name"`                      // 产品名称
	ProdInstIdExt  string    `json:"prod_inst_id_ext"`               // 接入系统产品实例ID
	ProdSnapshotId string    `json:"prod_snapshot_id"`               // 产品快照ID
	PlanId         string    `json:"plan_id"`                        // 定价方案ID
	IsAutoRenew    int64     `json:"is_auto_renew"`                  // 是否自动续订:0否,1是
	RenewDuration  string    `json:"renew_duration"`                 // 自动续约时长
	Status         string    `json:"status"`                         // 实例状态
	IsDeleted      int64     `json:"is_deleted"`                     // 是否删除:0否,1是
	CreatedBy      string    `json:"created_by"`                     // 创建人
	CreateTime     time.Time `json:"create_time"`                    // 创建时间
	UpdateTime     time.Time `json:"update_time"`                    // 更新时间
	ProdSnapshot   *ProdSnapshot
}

func (ProdInstance) TableName() string {
	return "prod_instance"
}

type ProdSnapshot struct {
	ProdSnapshotId string `json:"prod_snapshot_id" gorm:"primaryKey"`
	OriginRequest  string `json:"origin_request"`
	Attributes     string `json:"attributes"`
	Version        string `json:"version"`
}

func (ProdSnapshot) TableName() string {
	return "prod_snapshot"
}

func Run(dsn string) (err error) {
	var (
		prodInstances []*ProdInstance
		op            OrderProductV2
		db            *gorm.DB
		updateNum     int
	)

	if db, err = gorm.Open(mysql.Open(dsn), &gorm.Config{}); err != nil {
		return
	}

	if prodInstances, err = getProdInstances(db); err != nil {
		return
	}

	for _, pis := range prodInstances {
		if pis.ProdSnapshot == nil {
			continue
		}

		if err := json.Unmarshal([]byte(pis.ProdSnapshot.OriginRequest), &op); err != nil {
			log.Printf("json.Unmarshal err:%v\n", err)
		}
		for _, component := range op.Plan.Components {
			component.Attrs = append(component.Attrs, &AttrIdValue{
				AttrId: &wrappers.StringValue{
					Value: "atr_renew_price",
				},
				AttrValue: &wrappers.StringValue{
					Value: "0",
				},
			}, &AttrIdValue{
				AttrId: &wrappers.StringValue{
					Value: "patr_IsNew",
				},
				AttrValue: &wrappers.StringValue{
					Value: "true",
				},
			})
		}
		orByte, err := json.Marshal(op)
		if err != nil {
			log.Printf("json.Marshal err:%v\n", err)
		}

		pis.ProdSnapshot.OriginRequest = string(orByte)

		if err := db.Save(pis.ProdSnapshot).Error; err != nil {
			log.Printf("db.Save err:%v\n", err)
		}
		updateNum++
	}

	log.Printf("更新记录条数:%d\n", updateNum)

	return
}

func getProdInstances(db *gorm.DB) (prodInstances []*ProdInstance, err error) {
	//test
	//prod_inst_id_ext:=[]string{
	//	"MQ-20210609191702-500",
	//}

	prod_inst_id_ext := []string{
		"odri-00qxzvsk.dmn-cjhy8mlh.#@pek3",
		"odri-08kwvymd.dmn-v0px975r.#@pek3",
		"odri-0brxuzvt.dmn-u0mra9qw.#@pek3",
		"odri-0gk19sr7.dmn-snfahc9h.#@pek3",
		"odri-0oz2lc0g.dmn-2wvhkmxh.#@pek3",
		"odri-0wd3s8jw.dmn-d0xjxm2h.#@pek3",
		"odri-1agayjlg.dmn-jeic7fox.#@pek3",
		"odri-1fh7dwkb.dmn-w1ihy21q.#@pek3",
		"odri-1y25k871.dmn-z2c06op3.#@pek3",
		"odri-29xkxogi.dmn-a313qtly.#@pek3",
		"odri-2wqcreey.dmn-y5qlcm9h.#@pek3",
		"odri-3lhun90f.dmn-hd0ha8je.#@pek3",
		"odri-3tyoerab.dmn-rf1p5xts.#@pek3",
		"odri-3x39eih5.dmn-q6itkzsj.#@pek3",
		"odri-523p3o91.dmn-9vc2a5tm.#@pek3",
		"odri-57de7m0v.dmn-egob9zpe.#@pek3",
		"odri-57zcao16.dmn-fo66xeb6.#@pek3",
		"odri-5bpz0k7z.dmn-cvy2l1mj.#@pek3",
		"odri-5sbsxcau.dmn-0kbbqf9d.#@pek3",
		"odri-5yhgx781.dmn-elex63ln.#@pek3",
		"odri-6rccghlk.dmn-mi60ptzx.#@pek3",
		"odri-7c3o6ufa.dmn-csyij9t6.#@pek3",
		"odri-7mpyvti8.dmn-ywmx6e0u.#@pek3",
		"odri-80ic9328.dmn-8u7pgoqk.#@pek3",
		"odri-8aqj662g.dmn-ah8tu8re.#@pek3",
		"odri-8cmuzirm.dmn-kue32msx.#@pek3",
		"odri-9rwcjboe.dmn-buw8rnd8.#@pek3",
		"odri-9vbpjwn6.dmn-kwqm5kd8.#@pek3",
		"odri-9y6cx07q.dmn-hxmhro1i.#@pek3",
		"odri-adqe5yuf.dmn-jyy1yirg.#@pek3",
		"odri-atmrcafl.dmn-m172nlut.#@pek3",
		"odri-aykbkb5o.dmn-9odfvzqn.#@pek3",
		"odri-b883zo5v.dmn-1dy3ea63.#@pek3",
		"odri-bp2y0oba.dmn-kmdrkj3v.#@pek3",
		"odri-ca7prpwa.dmn-sredenif.#@pek3",
		"odri-cdk9l8pc.dmn-ohxdvyat.#@pek3",
		"odri-cjwqwnkt.dmn-5fx6ei06.#@pek3",
		"odri-cw5tylcq.dmn-9htozshu.#@pek3",
		"odri-d2gtjw2g.dmn-ensos81w.#@pek3",
		"odri-d2n7l0ts.dmn-ozheanr5.#@pek3",
		"odri-dgaqhmsj.dmn-jyhhz3gr.#@pek3",
		"odri-dmg0exat.dmn-yvfovs88.#@pek3",
		"odri-dupkjvrl.dmn-mjmgjefi.#@pek3",
		"odri-e6ivafo3.dmn-n8tmuoaf.#@pek3",
		"odri-e7opkrfz.dmn-5zhz8mpu.#@pek3",
		"odri-eecsnmsp.dmn-5rwup9z0.#@pek3",
		"odri-eilbemjw.dmn-b5wqee2i.#@pek3",
		"odri-elnv2vla.dmn-ez06hicp.#@pek3",
		"odri-es9coe55.dmn-lc9rgxvv.#@pek3",
		"odri-f1s6w330.dmn-06wbbsfg.#@pek3",
		"odri-f500nspu.dmn-qed7mhlv.#@pek3",
		"odri-f5jaa7hb.dmn-3jqz6m7e.#@pek3",
		"odri-foq5pofk.dmn-9zcbxgyp.#@pek3",
		"odri-ft8i50nk.dmn-h7nt1qj5.#@pek3",
		"odri-fv3okcdu.dmn-ub057yrn.#@pek3",
		"odri-fzqwzmus.dmn-eq310k5d.#@pek3",
		"odri-g3sbziyt.dmn-m2zzf8wr.#@pek3",
		"odri-gv0ocyv3.dmn-cs9oz8kn.#@pek3",
		"odri-gvyyqgen.dmn-htvm01if.#@pek3",
		"odri-h1cnrkk2.dmn-uj3wguhm.#@pek3",
		"odri-h7ncn5qg.dmn-y1ogmwrb.#@pek3",
		"odri-hbdnkf1s.dmn-12bdugxp.#@pek3",
		"odri-hjekustc.dmn-nmidem9e.#@pek3",
		"odri-i9ljjxyc.dmn-t0nvtbkj.#@pek3",
		"odri-jhvky52h.dmn-nrino6sl.#@pek3",
		"odri-jjrb7gkc.dmn-8g1dp55p.#@pek3",
		"odri-jnt8vjy8.dmn-uvjz0qdt.#@pek3",
		"odri-jo7iaon9.dmn-a0mbou55.#@pek3",
		"odri-jy731upo.dmn-hxk3i5i2.#@pek3",
		"odri-k75zzlj3.dmn-6icsqlm8.#@pek3",
		"odri-k7dndm70.dmn-hmvy9hwi.#@pek3",
		"odri-kjgxudu1.dmn-qq50eqrl.#@pek3",
		"odri-knd5lqrm.dmn-xh76v8xa.#@pek3",
		"odri-l98uwdw3.dmn-iinlooc2.#@pek3",
		"odri-latmaugx.dmn-kbau5oge.#@pek3",
		"odri-lgqvvfsb.dmn-8w5hvs86.#@pek3",
		"odri-lk8ji9a9.dmn-fx76wuui.#@pek3",
		"odri-loyy2yis.dmn-n5kd80ku.#@pek3",
		"odri-lrc8z63s.dmn-kgm3i9jl.#@pek3",
		"odri-m8i3nmh9.dmn-hiu8bs9s.#@pek3",
		"odri-mfybxqsk.dmn-hlt2qz1u.#@pek3",
		"odri-n0ucoxgp.dmn-bytonuxd.#@pek3",
		"odri-nc3fc7b6.dmn-w3g57mbw.#@pek3",
		"odri-o8ss6c5r.dmn-3xwm2qkf.#@pek3",
		"odri-o8z69wf2.dmn-g690pikb.#@pek3",
		"odri-of18t6en.dmn-9fu2j6b1.#@pek3",
		"odri-oiayw1do.dmn-q71xa3k8.#@pek3",
		"odri-onlgxyei.dmn-qasezxsg.#@pek3",
		"odri-p5lxknzh.dmn-b5arfn68.#@pek3",
		"odri-p66kbp9e.dmn-lkq7k3px.#@pek3",
		"odri-pigy1i66.dmn-5fzx3cpf.#@pek3",
		"odri-prdhwpep.dmn-dxcpvrc8.#@pek3",
		"odri-puj97m8o.dmn-df3az8ct.#@pek3",
		"odri-px0ex90e.dmn-z9x7sxcd.#@pek3",
		"odri-pzc6lmax.dmn-8yryma6p.#@pek3",
		"odri-pzivcyiw.dmn-eh3xrtsu.#@pek3",
		"odri-q7vliqni.dmn-jlype95m.#@pek3",
		"odri-qr8d36uz.dmn-aw90f8tp.#@pek3",
		"odri-rw5gs0l5.dmn-50qthaqi.#@pek3",
		"odri-szsdtips.dmn-3agr89j3.#@pek3",
		"odri-t2ixvjnz.dmn-3ygozb0z.#@pek3",
		"odri-tihyttek.dmn-vg97kvaq.#@pek3",
		"odri-tp2t61bw.dmn-nxq29ik6.#@pek3",
		"odri-ukw0eddf.dmn-ymkejqpk.#@pek3",
		"odri-uufb19lj.dmn-dvro6ra2.#@pek3",
		"odri-uv3nvz1j.dmn-aozzoytg.#@pek3",
		"odri-uy6iowyw.dmn-65hhzxza.#@pek3",
		"odri-v0k0099f.dmn-c91sfyc8.#@pek3",
		"odri-v2okmmko.dmn-zo2mx70l.#@pek3",
		"odri-v6lgyw9e.dmn-zbfd8vjx.#@pek3",
		"odri-v712rd7e.dmn-sw2d55qd.#@pek3",
		"odri-vg8b3fkr.dmn-zdu6e0tv.#@pek3",
		"odri-vh0z3c8e.dmn-n7blvh8n.#@pek3",
		"odri-vi8s5meo.dmn-oyi9i1cy.#@pek3",
		"odri-vjdvyngc.dmn-wxxjy3jn.#@pek3",
		"odri-vplhtinj.dmn-1i5d70tv.#@pek3",
		"odri-vs823p9p.dmn-tc79eiwx.#@pek3",
		"odri-vz2iqqvf.dmn-q9ahipk3.#@pek3",
		"odri-wbjbe5ad.dmn-vnxo6w8j.#@pek3",
		"odri-wg1zqw59.dmn-f5y76fpv.#@pek3",
		"odri-wjbwddq3.dmn-0uhdy5ug.#@pek3",
		"odri-wn6pi0i0.dmn-nc5x99g5.#@pek3",
		"odri-wq0stmpo.dmn-z8sj2yig.#@pek3",
		"odri-wtccqo0x.dmn-5ddyero8.#@pek3",
		"odri-wvnpq2qm.dmn-xkuu375s.#@pek3",
		"odri-x1v2p8nl.dmn-qtdokbsg.#@pek3",
		"odri-x6wvyg5c.dmn-ov8ht8it.#@pek3",
		"odri-xcmxy7lx.dmn-wajb1y77.#@pek3",
		"odri-xdq0cuq0.dmn-rytjla3g.#@pek3",
		"odri-xmnakm8g.dmn-zwrz8kwh.#@pek3",
		"odri-xpjusz5q.dmn-ox65u5qs.#@pek3",
		"odri-xuyrv8ws.dmn-srwqdw0v.#@pek3",
		"odri-xyp1q8i2.dmn-syfnmbdq.#@pek3",
		"odri-y6ghtrjt.dmn-5pcgz88h.#@pek3",
		"odri-yfdnp3ya.dmn-nnbufpov.#@pek3",
		"odri-yitxqbqm.dmn-hbdgfwk6.#@pek3",
		"odri-yoql2pw6.dmn-nlae76mv.#@pek3",
		"odri-z0aitnlh.dmn-f8pglee9.#@pek3",
		"odri-zggni0ii.dmn-npnxsy3x.#@pek3",
		"odri-zksendew.dmn-axa3ofhc.#@pek3",
		"odri-zs5aqdb0.dmn-7bfe6med.#@pek3",
		"odri-zsvl9zk5.dmn-2buclrtj.#@pek3",
		"odri-zsylg1px.dmn-nyxfe1k8.#@pek3",
	}

	log.Printf("查询记录条数:%d\n", len(prod_inst_id_ext))
	db.Select("prod_snapshot_id").Preload("ProdSnapshot").Where("prod_inst_id_ext", prod_inst_id_ext).Find(&prodInstances)
	log.Printf("查询记录结果条数:%d\n", len(prodInstances))

	return
}

//json方式打印结构体
func PrintJson(obj interface{}) {
	tmp, _ := json.MarshalIndent(obj, "", "     ")
	fmt.Println(string(tmp))
}
