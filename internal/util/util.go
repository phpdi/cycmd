package util

import (
	"encoding/json"
	"fmt"
)

//json方式打印结构体
func PrintJson(obj interface{}) {
	tmp, _ := json.MarshalIndent(obj, "", "  ")
	fmt.Println(string(tmp))
}

func Contains[T comparable](ss []T, s T) bool {
	for _, v := range ss {
		if v == s {
			return true
		}
	}
	return false
}
